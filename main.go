package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"io"
	"log"
	"net/http"
	"os"
)

func initBotToken() {
	if err := godotenv.Load(); err != nil {
		log.Println("No .env file found")
	}
}

func getData(method string) (resp *http.Response) {
	botToken, _ := os.LookupEnv("botToken")
	st := "https://api.telegram.org/bot" + botToken + "/" + method

	resp, err := http.Get(st)
	if err != nil {
		log.Println(err)
	}
	return
}

func setLogFile(st string) {
	file, err := os.OpenFile(st, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalln("error opening the log file")
	}
	defer file.Close()

	log.SetOutput(file)
	log.Println("Log file is set")
}

func decodeRequest(r io.Reader) (st string) {
	binSt, _ := io.ReadAll(r)
	st = string(binSt)
	return
}

func writeLogs(r io.Reader) {
	st := decodeRequest(r)
	log.Println(st)
}

func showLogs(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/plain")

	f, err := os.Open("logs")
	if err != nil {
		log.Fatalln(err)
	}
	binSt, err := io.ReadAll(f)

	_, err = w.Write(binSt)
	if err != nil {
		log.Fatalln(err)
	}
}

func webHook(w http.ResponseWriter, r *http.Request) {
	writeLogs(r.Body)
	showLogs(w)
	return
}

func main() {
	initBotToken()
	setLogFile("logs")

	r := mux.NewRouter()

	r.HandleFunc("/vuzobot_webhook", webHook)

	st := "https://api.telegram.org/bot" + "5449185173:AAH5frYk3o4kS4zJRBSUYmzYj1CEomI-coU" + "/" + "setWebhook" +
		"?url=https://gihub.spb.ru/vuzobot_webhook"

	fmt.Println(st)
	resp, err := http.Get(st)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(resp)

	err = http.ListenAndServe(":8088", r)
	if err != nil {
		log.Println(err)
	}
}
