FROM golang:1.18

RUN mkdir /app
ADD . /app/
WORKDIR /app

RUN go mod download

RUN go build -o main .
RUN ls
EXPOSE 8088
CMD ["./main"]


